//
//  STKP2AppDelegate.h
//  StackpopAgent
//
//  Created by Jacob Budin on 2/2/14.
//  Copyright (c) 2014 Jacob Budin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface STKP2AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
