//
//  STKP2AppDelegate.m
//  Stackpop 2
//
//  Created by Jacob Budin on 2/2/14.
//  Copyright (c) 2014 Jacob Budin. All rights reserved.
//

#import "STKP2AppDelegate.h"

@implementation STKP2AppDelegate

float loadInterval = 60*5;

- (id)init{
    self = [super init];
    if (self) {
        _notificationTypes = [[NSMutableArray alloc] init];
        _notificationsShown = [[NSMutableArray alloc] init];
        _inboxItemsShown = [[NSMutableArray alloc] init];
        _reputationChangesShown = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)awakeFromNib{
    _statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength];
    NSImage *statusBarIcon = [[NSImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"status-bar-icon" ofType:@"png"]];
    NSImage *altStatusBarIcon = [[NSImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"status-bar-icon-alt" ofType:@"png"]];
    [statusBarIcon setSize:(NSSize){20,16}];
    [altStatusBarIcon setSize:(NSSize){20,16}];
    [_statusItem setMenu:[self menu]];
    //[_statusItem setTitle:@"Status"];
    [statusBarIcon setTemplate:YES]; //Allows AppKit to handle styling-- useful for Yosemite Dark Mo
    [_statusItem setImage:statusBarIcon];
    [_statusItem setAlternateImage:altStatusBarIcon];
    [_statusItem setHighlightMode:YES];
}


- (void)userNotificationCenter:(NSUserNotificationCenter *)center didActivateNotification:(NSUserNotification *)notification{
    NSString *notificationUrl = [[notification userInfo] objectForKey:@"url"];
    if (notificationUrl) {
        [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:notificationUrl]];
    }
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    float now = [[NSDate date]timeIntervalSince1970];
    [self setTimeStarted:((int)now)];
    
    [[NSUserNotificationCenter defaultUserNotificationCenter] setDelegate:self];
    
    // Check authorization window
    NSMethodSignature *timerSignature = [[self class] instanceMethodSignatureForSelector:@selector(load:)];
    NSInvocation *timerInvocation = [NSInvocation invocationWithMethodSignature:timerSignature];
    [timerInvocation setTarget:self];
    [timerInvocation setSelector:@selector(load:)];
    double timerTimeInterval = STKP2LoadInterval;
    
    NSTimer *loadTimer = [NSTimer scheduledTimerWithTimeInterval:timerTimeInterval invocation:timerInvocation repeats:YES];
    [self setLoadTimer:loadTimer];
}

- (void)updateNotificationTypes{
    [self setNotificationTypes:[NSMutableArray array]];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [self setPrimarySiteName:[defaults objectForKey:@"com.Budin.Stackpop.primarySiteName"]];
    [self setPrimarySiteUrl:[defaults objectForKey:@"com.Budin.Stackpop.primarySiteUrl"]];
    
    NSArray* notificationTypeList = @[@"answers", @"messages", @"badges", @"comments", @"reputation", @"privileges", @"suggestedEdits"];
    NSString* prefix = @"com.Budin.Stackpop.notify";
    
    for (NSString* notificationType in notificationTypeList) {
        NSString *notificationKey = [prefix copy];
        notificationKey = [notificationKey stringByAppendingString:[[notificationType substringToIndex:1] uppercaseString]];
        notificationKey = [notificationKey stringByAppendingString:[notificationType substringFromIndex:1]];
        if ([defaults boolForKey:notificationKey]) {
            [[self notificationTypes] addObject:notificationType];
        }
    }
}

- (NSString*)accessToken{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //[defaults addSuiteNamed:@"com.Budin.Stackpop"];
    
    if (!_accessToken) {
        NSString* accessToken = [defaults objectForKey:@"com.Budin.Stackpop.accessToken"];
        
        if (accessToken && ([accessToken length] > 0)) {
            _accessToken = accessToken;
        }
    }
    
    return _accessToken;
}

- (void)setAccessToken:(NSString *)accessToken{
    _accessToken = accessToken;
}
- (void)showNotification:(NSString*)title withSubtitle:(NSString*)subtitle withText:(NSString*)text withUrl:(NSString*)url{
    title = [self stripTags:title];
    subtitle = [self stripTags:subtitle];
    text = [self stripTags:text];
    title = [title gtm_stringByUnescapingFromHTML];
    subtitle = [subtitle gtm_stringByUnescapingFromHTML];
    text = [text gtm_stringByUnescapingFromHTML];
    
    NSUserNotification *notification = [[NSUserNotification alloc] init];
    NSDictionary *info = @{@"url": url};
    [notification setUserInfo:info];
    [notification setTitle:title];
    [notification setSubtitle:subtitle];
    [notification setInformativeText:text];
    [notification setSoundName:NSUserNotificationDefaultSoundName];
    
    [[NSUserNotificationCenter defaultUserNotificationCenter] deliverNotification:notification];
}

- (void)load:(id)sender{
    [self updateNotificationTypes];
    
    if ([[self notificationTypes] indexOfObject:@"badges"] != NSNotFound) {
        [self request:@"/notifications/unread" executeWithSelector:@selector(loadNotifications:)];
    }
    
    if ([[self notificationTypes] indexOfObject:@"comments"] != NSNotFound) {
        [self request:@"/inbox/unread" executeWithSelector:@selector(loadInbox:)];
    }
    
    if ([self primarySiteUrl] && [[self notificationTypes] indexOfObject:@"reputation"] != NSNotFound) {
        NSURL *url = [NSURL URLWithString:[self primarySiteUrl]];
        NSString *primarySiteHost = [url host];
        NSString *requestUrl = [NSString stringWithFormat:@"/me/reputation-history/full?site=%@", primarySiteHost];
        [self request:requestUrl executeWithSelector:@selector(loadReputationHistory:)];
    }
}

- (void)loadInbox:(NSArray*)items{
    NSDictionary *types = @{
                            @"comment":@"New Comment",
                            @"chat_message":@"Chat Message",
                            @"new_answer":@"New Answer",
                            @"meta_question":@"New Meta Question"
                            };
    
    for (NSDictionary* item in items) {
        NSNumber *createdTime = [item objectForKey:@"creation_date"];
        
        if ([createdTime integerValue] < [self timeStarted]) {
            continue;
        }
        
        NSString* itemType = [item objectForKey:@"item_type"];
        
        if (([itemType isEqualToString:@"comment"]) && ([[self notificationTypes] indexOfObject:@"comments"] == NSNotFound)) {
            continue;
        }
        if (([itemType isEqualToString:@"chat_message"]) && ([[self notificationTypes] indexOfObject:@"messages"] == NSNotFound)) {
            continue;
        }
        if (([itemType isEqualToString:@"new_answer"]) && ([[self notificationTypes] indexOfObject:@"answers"] == NSNotFound)) {
            continue;
        }
        /*
         if (([itemType isEqualToString:@"meta_question"]) && ([[self notificationTypes] indexOfObject:@"metaQuestions"] == NSNotFound)) {
         continue;
         }
         */
        
        NSString* title = [types objectForKey:itemType];
        NSString* uniqueKey = [title stringByAppendingString:[createdTime stringValue]];
        
        if (title && ([[self inboxItemsShown] indexOfObject:uniqueKey] == NSNotFound)) {
            [[self inboxItemsShown] addObject:uniqueKey];
            
            NSString* siteName = [[item objectForKey:@"site"] objectForKey:@"name"];
            NSString* text = [item objectForKey:@"title"];
            NSString* url = [item objectForKey:@"link"];
            
            [self showNotification:siteName
                      withSubtitle:title
                          withText:text
                           withUrl:url];
        }
    }
}

- (IBAction)openPreferences:(id)sender {
    if([self preferencesDelegate] == nil || [[self preferencesDelegate] window] == nil){
        STKP2PreferencesDelegate *preferencesDelegate = [[STKP2PreferencesDelegate alloc] initWithWindowNibName:@"Preferences"];
        [[preferencesDelegate window] setDelegate:preferencesDelegate];
        [self setPreferencesDelegate:preferencesDelegate];
    }
    [[self preferencesDelegate] showWindow:self];
    //[[[self preferencesDelegate] window] makeKeyAndOrderFront:[[self preferencesDelegate] window]];
    //[[[self preferencesDelegate] window] setOrderedIndex:0];
    //[[NSApp mainWindow] makeKeyWindow];
    //[NSApp activateIgnoringOtherApps:YES];
}

- (void)loadNotifications:(NSArray*)items{
    NSDictionary *types = @{
                            @"badge_earned":@"Badge Earned",
                            @"reputation_bonus":@"Reputation Bonus",
                            @"new_privilege":@"New Privilege",
                            @"edit_suggested":@"Suggested Edit"
                            };
    
    for (NSDictionary* item in items) {
        NSNumber *createdTime = [item objectForKey:@"creation_date"];
        
        if ([createdTime integerValue] < [self timeStarted]) {
            continue;
        }
        
        NSString* itemType = [item objectForKey:@"notification_type"];
        
        if (([itemType isEqualToString:@"badge_earned"]) && ([[self notificationTypes] indexOfObject:@"badges"] == NSNotFound)) {
            continue;
        }
        if (([itemType isEqualToString:@"reputation_bonus"]) && ([[self notificationTypes] indexOfObject:@"reputation"] == NSNotFound)) {
            continue;
        }
        if (([itemType isEqualToString:@"new_privilege"]) && ([[self notificationTypes] indexOfObject:@"privileges"] == NSNotFound)) {
            continue;
        }
        if (([itemType isEqualToString:@"edit_suggested"]) && ([[self notificationTypes] indexOfObject:@"suggestedEdits"] == NSNotFound)) {
            continue;
        }
        
        NSString* title = [types objectForKey:itemType];
        NSString* uniqueKey = [title stringByAppendingString:[createdTime stringValue]];
        
        if (title && ([[self notificationsShown] indexOfObject:uniqueKey] == NSNotFound)) {
            [[self notificationsShown] addObject:uniqueKey];
            
            NSString* siteName = [[item objectForKey:@"site"] objectForKey:@"name"];
            NSString* siteUrl = [[item objectForKey:@"site"] objectForKey:@"site_url"];
            NSString* text = [item objectForKey:@"body"];
            
            [self showNotification:siteName
                      withSubtitle:title
                          withText:text
                           withUrl:siteUrl];
        }
    }
}

- (IBAction)quit:(id)sender {
    [NSApp terminate:self];
}

/*
 - (NSDictionary*)loadPost:(NSInteger)postId{
 
 }
 */

- (void)loadReputationHistory:(NSArray*)items{
    if ([[self notificationTypes] indexOfObject:@"reputation"] == NSNotFound) {
        return;
    }
    
    NSDictionary *types = @{
                            @"post_upvoted":@"Post Upvoted",
                            @"suggested_edit_approval_received":@"Suggested Edit Approved",
                            @"answer_accepted":@"Answer Accepted",
                            @"bounty_earned":@"Bounty Earned",
                            };
    NSArray *typeKeys = [types allKeys];
    
    for (NSDictionary* item in items) {
        NSNumber *createdTime = [item objectForKey:@"creation_date"];
        
        if ([createdTime integerValue] < [self timeStarted]) {
            continue;
        }
        
        
        NSNumber* reputationChange = [item objectForKey:@"reputation_change"];
        NSNumber* postId = [item objectForKey:@"post_id"];
        NSString* uniqueKey = [[postId stringValue] stringByAppendingString:[createdTime stringValue]];
        NSString* reputationHistoryType = [item objectForKey:@"reputation_history_type"];
        
        NSString* subtitle = @"Reputation Change";
        
        if ([typeKeys indexOfObject:reputationHistoryType] != NSNotFound) {
            subtitle = [types objectForKey:reputationHistoryType];
        }
        
        if (postId && ([[self reputationChangesShown] indexOfObject:uniqueKey] == NSNotFound)) {
            [[self reputationChangesShown] addObject:uniqueKey];
            
            int reputationChangeInt = [reputationChange intValue];
            int reputationPositive = (reputationChangeInt > 0) ? reputationChangeInt : (reputationChangeInt*-1);
            NSString* text = @"You ";
            
            if (reputationChangeInt == 0) {
                continue;
            }
            else if (reputationChangeInt > 0) {
                text = [text stringByAppendingString:@"earned "];
                text = [text stringByAppendingString:[reputationChange stringValue]];
            }
            else{
                text = [text stringByAppendingString:@"lost "];
                text = [text stringByAppendingString:[[NSNumber numberWithInt:reputationPositive] stringValue]];
            }
            
            text = [text stringByAppendingString:@" reputation point"];
            if (reputationPositive != 1) {
                text = [text stringByAppendingString:@"s"];
            }
            text = [text stringByAppendingString:@"."];
            NSString *url = [NSString stringWithFormat:@"%@/q/%@", [self primarySiteUrl], [postId stringValue]];
            
            [self showNotification:[self primarySiteName]
                      withSubtitle:subtitle
                          withText:text
                           withUrl:url];
        }
    }
}

- (void)request:(NSString*)path executeWithSelector:(SEL)selector{
    NSRange hasQuery = [path rangeOfString:@"?"];
    if (hasQuery.location == NSNotFound) {
        path = [path stringByAppendingString:@"?"];
    }
    else{
        path = [path stringByAppendingString:@"&"];
    }
    
    NSString *s = [NSString stringWithFormat:@"https://api.stackexchange.com/2.1%@access_token=%@&key=%@&pagesize=10",
                   path,
                   [[self accessToken] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                   [STKP2OAuthKey stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSURL *url = [NSURL URLWithString:s];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    NSOperationQueue *q = [NSOperationQueue mainQueue];
    [NSURLConnection sendAsynchronousRequest:req queue:q completionHandler:^(NSURLResponse *resp, NSData *d, NSError *err) {
        if (d) {
            NSError *requestError;
            NSMutableDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:d options:kNilOptions error:&requestError];
            NSArray *items = [jsonResponse objectForKey:@"items"];
            [self performSelector:selector withObject:items];
        }
    }];
}

- (BOOL)userNotificationCenter:(NSUserNotificationCenter *)center
     shouldPresentNotification:(NSUserNotification *)notification
{
    return YES;
}

- (NSString*)stripTags:(NSString*)str
{
    NSMutableString *ms = [NSMutableString stringWithCapacity:[str length]];
    
    NSScanner *scanner = [NSScanner scannerWithString:str];
    [scanner setCharactersToBeSkipped:nil];
    NSString *s = nil;
    while (![scanner isAtEnd])
    {
        [scanner scanUpToString:@"<" intoString:&s];
        if (s != nil)
            [ms appendString:s];
        [scanner scanUpToString:@">" intoString:NULL];
        if (![scanner isAtEnd])
            [scanner setScanLocation:[scanner scanLocation]+1];
        s = nil;
    }
    
    return ms;
}

@end
